import java.util.Random;

public class Deck {
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck() {
		this.rng = new Random();
		
		this.numberOfCards = 52;
		
		this.cards = new Card[numberOfCards];
		
		String[] suits = {"Hearts", "Spades", "Diamonds", "Clubs"};
		
		String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};

		int k = 0;
		for(int i = 0; i < values.length; i++) {
			for(int j = 0; j < suits.length; j++) {
				this.cards[k] = new Card(values[i], suits[j]);
				k++;
			}
		}
	}
	
	public int length() {
		return this.numberOfCards;
	}
	
	public Card drawTopCard() {
		this.numberOfCards--;
		return cards[this.numberOfCards];
	}
	
	public String toString() {
		String allCards = "";
		for(int i = 0; i < this.numberOfCards; i++) {
			allCards += this.cards[i] + "\n";
		}
		return allCards;
	}
	
	public void shuffle() {
		for(int i = 0; i < this.numberOfCards; i++) {
			int randPos = i + rng.nextInt(this.numberOfCards - i);
			Card currentPos = this.cards[i];
			this.cards[i] = this.cards[randPos];
			this.cards[randPos] = currentPos;
		}
	}
}