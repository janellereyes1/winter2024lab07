public class Card {
	private String value;
	private String suit;

	public Card(String value, String suit) {
		this.value = value;
		this.suit = suit;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getSuit() {
		return this.suit;
	}
	
	public String toString() {
		return this.value + " of " + this.suit;
	}
	
	public double calculateScore() {
		String[] suits = {"Clubs", "Diamonds", "Spades", "Hearts"};
		String[] values = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		double score = 0;
		
		int cardValue = 0;
		for(int i = 0; i < values.length ; i++) {
			cardValue++;
			values[i] = cardValue;
			//score += cardValue;
		}
		score += cardValue;
		
		double suitValue = 0;
		for(int i = 0; i < suits.length ; i++) {
			suitValue = suitValue + 0.1;
			suits[i].equals(suitValue);
			//score += suitValue;
		}
		score += suitValue;
		
		//score += score + value + suit;
		return score;
		
	}
}