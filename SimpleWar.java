public class SimpleWar {
	public static void main(String[] args){
		Deck drawPile = new Deck();
		drawPile.shuffle();
		//System.out.println(drawPile);
		
		int pointsP1 = 0;
		int pointsP2 = 0;
		
		Card card1 = drawPile.drawTopCard();
		System.out.println(card1);
		double score1 = card1.calculateScore();
		System.out.println(score1);
		
		Card card2 = drawPile.drawTopCard();
		System.out.println(card2);
		double score2 = card2.calculateScore();
		System.out.println(score2);
		
		System.out.println("Player1 currently has: " + pointsP1 + " points!");
		if(score1 > score2) {
			System.out.println(card1 + " wins!");
			pointsP1++;
		}
		System.out.println("Player1 now has: " + pointsP1 + " points!");
		
		System.out.println("Player2 currently has: " + pointsP2 + " points!");
		if(score2 > score1) {
				System.out.println(card2 + " wins!");
				pointsP2++;
		}
		System.out.println("Player2 now has: " + pointsP2 + " points!");
		
		while(drawPile.length() > 1) {
			
		}
	}
	
}